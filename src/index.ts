import xs, {Stream, Listener} from 'xstream';
import {adapt} from '@cycle/run/lib/adapt';
const Notification = require('react-native-android-local-notification');

export type Attributes = {
  // Basics
  id?: number;
  subject?: string;
  message: string;
  action?: string;
  payload?: any;

  // Scheduling
  delay?: number;
  sendAt?: Date;
  repeatEvery?: string | number;
  repeatCount?: number;
  endAt?: Date;

  // Customization
  priority?: -2 | -1 | 0 | 1 | 2;
  smallIcon?: string;
  sound?: string;
  vibrate?: string | null;
  lights?: string | null;
  autoClear?: boolean;
  onlyAlertOnce?: boolean;
  tickerText?: string;
  when?: Date;
  bigText?: string;
  bigStyleImageBase64?: string;
  bigStyleUrlImgage?: string;
  subText?: string;
  progress?: number;
  color?: string;
  number?: number;
  category?:
    | 'alarm'
    | 'call'
    | 'email'
    | 'event'
    | 'progress'
    | 'reminder'
    | 'social';
  localOnly?: boolean;
};

export type DefaultCommand = {type: undefined} & Attributes;

export type CreateCommand = {type: 'create'} & Attributes;

export type DeleteCommand = {type: 'delete'; id: number};

export type DeleteAllCommand = {type: 'deleteAll'};

export type ClearCommand = {type: 'clear'; id: number};

export type ClearAllCommand = {type: 'clearAll'};

export type Command =
  | DefaultCommand
  | CreateCommand
  | DeleteCommand
  | DeleteAllCommand
  | ClearCommand
  | ClearAllCommand;

export type PressEvent = any;

export function notificationDriver(cmd$: Stream<Command>): Stream<PressEvent> {
  cmd$.addListener({
    next(cmd) {
      if (!cmd.type || cmd.type === 'create') {
        Notification.create(cmd);
      }
      if (cmd.type === 'delete') {
        Notification.delete(cmd.id);
      }
      if (cmd.type === 'deleteAll') {
        Notification.deleteAll();
      }
      if (cmd.type === 'clear') {
        Notification.clear(cmd.id);
      }
      if (cmd.type === 'clearAll') {
        Notification.clearAll();
      }
    },
  });

  return adapt(
    xs.create<PressEvent>({
      start(listener: Listener<PressEvent>) {
        Notification.addListener('press', (ev: PressEvent) => {
          listener.next(ev);
        });
      },
      stop() {
        Notification.removeAllListeners('press');
      },
    }),
  );
}
