# Cycle Native Android Local Notification

**A Cycle.js Driver for creating Android notifications in React Native using [react-native-android-local-notification](https://github.com/staltz/react-native-android-local-notification)**

```
npm install cycle-native-android-local-notification
```

**Note!** Follow the instructions given by react-native-android-local-notification to link the library correctly on the native (Java) side.

## Usage

### Source

Stream of press events for presses on any notification.

### Sink

Stream of command objects. The possible command types are

#### Create command

```
{
  type?: 'create',

  // Basics
  id?: number,
  subject?: string,
  message: string,
  action?: string,
  payload?: any,

  // Scheduling
  delay?: number,
  sendAt?: Date,
  repeatEvery?: string | number,
  repeatCount?: number,
  endAt?: Date,

  // Customization
  priority?: -2 | -1 | 0 | 1 | 2,
  smallIcon?: string,
  sound?: string,
  vibrate?: string | null,
  lights?: string | null,
  autoClear?: boolean,
  onlyAlertOnce?: boolean,
  tickerText?: string,
  when?: Date,
  bigText?: string,
  bigStyleImageBase64?: string,
  bigStyleUrlImgage?: string,
  subText?: string,
  progress?: number,
  color?: string,
  number?: number,
  category?:
    | 'alarm'
    | 'call'
    | 'email'
    | 'event'
    | 'progress'
    | 'reminder'
    | 'social',
  localOnly?: boolean,
}
```

#### Clear command

```
{
  type: 'clear',
  id: number,
}
```

#### ClearAll command

```
{
  type: 'clearAll',
}
```
#### Delete command

```
{
  type: 'delete',
  id: number,
}
```

#### DeleteAll command

```
{
  type: 'deleteAll',
}
```

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license

